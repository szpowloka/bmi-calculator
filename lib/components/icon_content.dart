import 'package:flutter/material.dart';
import 'constants.dart';

class GenderButton extends StatelessWidget {
  const GenderButton({this.genderIcon, this.genderText});

  final String genderText;
  final IconData genderIcon;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          genderIcon,
          size: 80.0,
          color: Colors.white,
        ),
        SizedBox(
          height: 15.0,
        ),
        Text(
          genderText,
          style: kSmallTextStyle,
        ),
      ],
    );
  }
}

