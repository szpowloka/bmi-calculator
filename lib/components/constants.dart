import 'package:flutter/material.dart';

const kSmallTextStyle = TextStyle(
  fontSize: 18.0,
  color: Color(0xFF8D8E98),
);

const kBigTextStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.w900,
);

const kLargeButtonStyle = TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
);

const double kBottomBarHeight = 80.0;
const kButtonColor = Color(0xFF1D1E33);
const kInactiveButtonColor = Color(0xFF111328);